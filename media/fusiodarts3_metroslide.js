$( document ).ready(function() {
    $(".metroslide .tile").height($("#tile1").width());
    $(".metroslide .carousel").height($("#tile1").width());
    $(".metroslide .item").height($("#tile1").width());

    $(window).resize(function() {
        if(this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 10);
    });
    
    $(window).bind('resizeEnd', function() {
    	$(".metroslide .tile").height($("#tile1").width());
        $(".metroslide .carousel").height($("#tile1").width());
        $(".metroslide .item").height($("#tile1").width());
    });

});