<?php
/*------------------------------------------------------------------------
# fusiodarts_blank.php
# ------------------------------------------------------------------------
# version		3.0.0
# author    	Angel Albiach - Fusió d'Arts Technology S.L.
# copyright 	Copyright (c) 2016 Fusió d'Arts All rights reserved.
# @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website		http://www.fusiodarts.com
-------------------------------------------------------------------------
*/

defined('_JEXEC') or die;

if ($params->def('prepare_content', 1))
{
	JPluginHelper::importPlugin('content');
	$module->content = JHtml::_('content.prepare', $module->content, '', 'mod_fusiodarts3_metroslide.content');
} 

if ($params->get('active_bootstrap', 1)) { ?>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<?php }

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base(true) . '/media/mod_fusiodarts3_metroslide/media/fusiodarts3_metroslide.css');
$document->addScript(JURI::base(true) . '/media/mod_fusiodarts3_metroslide/media/fusiodarts3_metroslide.js');

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', $params->get('layout', 'default'));
