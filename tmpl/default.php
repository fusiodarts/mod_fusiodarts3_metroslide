<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_fusiodarts3_metroslide
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
// SECTION 1
$s1_style = json_decode($params->get('s1_style'));
$s1_item1 = json_decode($params->get('s1_item1'));
$s1_item2 = json_decode($params->get('s1_item2'));
$s1_item3 = json_decode($params->get('s1_item3'));
$s1_item4 = json_decode($params->get('s1_item4'));

// SECTION 2
$s2_style = json_decode($params->get('s2_style'));
$s2_item1 = json_decode($params->get('s2_item1'));
$s2_item2 = json_decode($params->get('s2_item2'));
$s2_item3 = json_decode($params->get('s2_item3'));
$s2_item4 = json_decode($params->get('s2_item4'));

// SECTION 3
$s3_style = json_decode($params->get('s3_style'));
$s3_item1 = json_decode($params->get('s3_item1'));
$s3_item2 = json_decode($params->get('s3_item2'));
$s3_item3 = json_decode($params->get('s3_item3'));
$s3_item4 = json_decode($params->get('s3_item4'));

$secciones['s1']['style'] = $s1_style;
$secciones['s1']['item1'] = $s1_item1;
$secciones['s1']['item2'] = $s1_item2;
$secciones['s1']['item3'] = $s1_item3;
$secciones['s1']['item4'] = $s1_item4;

$secciones['s2']['style'] = $s2_style;
$secciones['s2']['item1'] = $s2_item1;
$secciones['s2']['item2'] = $s2_item2;
$secciones['s2']['item3'] = $s2_item3;
$secciones['s2']['item4'] = $s2_item4;

$secciones['s3']['style'] = $s3_style;
$secciones['s3']['item1'] = $s3_item1;
$secciones['s3']['item2'] = $s3_item2;
$secciones['s3']['item3'] = $s3_item3;
$secciones['s3']['item4'] = $s3_item4;

$sections = json_decode($params->get('sections'));

?>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<div class="dynamicTile metroslide">
    <div class="row ">

    <?php foreach ($secciones as $sec) : ?>
        <?php if ($sec['style'] == '1') { ?>
            <div class="col-xs-<?php echo 12/$sections; ?>">
                <div class="row ">
                    <div class="col-xs-6">
                        <div id="tile1" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="5000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item1');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="tile2" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="7000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item2');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="tile7" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="9000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item3');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        
        <?php if ($sec['style'] == '2') { ?>
            <div class="col-xs-<?php echo 12/$sections; ?>">
                <div class="row ">
                    <div class="col-xs-6">
                        <div id="tile3" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="7000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item1');
                                    endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="tile4" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="5000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item2');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div id="tile8" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="5000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item3');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="tile9" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="9000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item4');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        
        <?php if ($sec['style'] == '3') { ?>
            <div class="col-xs-<?php echo 12/$sections; ?>">
                <div class="row ">
                    <div class="col-xs-12">
                        <div id="tile10" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="7000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item1');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div id="tile5" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="5000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item2');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="tile6" class="tile">

                            <div class="carousel slide" data-ride="carousel" data-interval="7000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php foreach ($sec as $item) :
                                        require JModuleHelper::getLayoutPath('mod_fusiodarts3_metroslide', 'default_item3');
                                    endforeach; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php endforeach; ?>
    </div>
</div>