<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_fusiodarts3_metroslide
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

if ($item->s1_i4_image) {
    $item_image = $item->s1_i4_image;
    $item_text = $item->s1_i4_text;
    $item_color = $item->s1_i4_color;
    $count = count($item->s1_i4_image);
    $item_link = $item->s1_i4_link;
} else if ($item->s2_i4_image) {
    $item_image = $item->s2_i4_image;
    $item_text = $item->s2_i4_text;
    $item_color = $item->s2_i4_color;
    $count = count($item->s2_i4_image);
    $item_link = $item->s2_i4_link;
} else if ($item->s3_i4_image) {
    $item_image = $item->s3_i4_image;
    $item_text = $item->s3_i4_text;
    $item_color = $item->s3_i4_color;
    $count = count($item->s3_i4_image);
    $item_link = $item->s3_i4_link;
}

if ($item_image != null) {
    for ($i = 0; $i < $count; $i++) {
        if ($i == 0) {
            $active = 'active';
        } else {
            $active = '';
        } ?>
        <div class="item <?php echo $active; ?>" style="background-color: <?php echo $item_color[$i]; ?>">
            <?php if (!empty($item_link[$i])) { ?>
                <a href="<?php echo $item_link[$i]; ?>" target="_blank">
            <?php } ?>
                <?php if ($item_image[$i] != '') { ?>
                    <img src="<?php echo $item_image[$i]; ?>" class="img-responsive"/>
                <?php } else if ($item_text[$i] != '') { ?>
                    <p class="title-caption"><?php echo $item_text[$i]; ?></p>
                <?php } ?>
            <?php if (!empty($item_link)) { ?>
                </a>
            <?php } ?>
        </div>
    <?php unset($item_image[$i]);
    } ?>
    <?php
}